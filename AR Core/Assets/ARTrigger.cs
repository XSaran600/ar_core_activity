﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ARTrigger : MonoBehaviour
{
    public ParticleSystem p;

    private void Start()
    {
        p.Stop();
    }

    private void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == "Player")
        {
            if (!p.isPlaying)
            {
                p.Play();
                Debug.Log("play");
            }
            Debug.Log("col");
        }
    }
}
